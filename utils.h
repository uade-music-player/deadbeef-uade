#ifndef _DEADBEEF_UADE_UTILS_H_
#define _DEADBEEF_UADE_UTILS_H_

#include <stdlib.h>
#include <stdio.h>

#define debug(fmt, args...) do { fprintf(stderr, fmt, ## args); } while (0)
#define error(fmt, args...) do { fprintf(stderr, "deadbeef-uade error: " fmt, ## args); } while(0)
#define info(fmt, args...) do { fprintf(stderr, fmt, ## args); } while (0)
#define warning(fmt, args...) do { fprintf(stderr, "deadbeef-uade warning: " fmt, ## args); } while(0)

#define free_and_null(x) do { free(x); (x) = NULL; } while (0)
#define free_and_poison(x) do { free(x); (x) = (void *) -1; } while (0)

#endif
