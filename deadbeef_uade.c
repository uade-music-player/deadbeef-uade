/*
 * uade plugin for deadbeef
 * Copyright (C) 2013 Heikki Orsila <heikki.orsila@iki.fi>
 */

#include "utils.h"

#include <bencodetools/bencode.h>
#include <deadbeef/deadbeef.h>
#include <uade/uade.h>

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static DB_functions_t *deadbeef;
static DB_decoder_t plugin;

struct uade_info_t {
	DB_fileinfo_t info;
	struct uade_state *state;
	size_t bytesleft;
};

static DB_fileinfo_t *deadbeef_uade_open(uint32_t hints)
{
	struct uade_info_t *info;
	DB_fileinfo_t *_info = malloc(sizeof(*info));
	info = (struct uade_info_t *) _info;
	memset(info, 0, sizeof *info);
	return _info;
}

static const char *get_fname(DB_playItem_t *it)
{
	const char *fname;
	deadbeef->pl_lock ();
	fname = deadbeef->pl_find_meta(it, ":URI");
	deadbeef->pl_unlock ();
	return fname;
}

/* Pass subsong == -1 if you want the default subsong */
static struct uade_state *play_file(const char *fname, int subsong)
{
	size_t size;
	size_t nmemb;
	DB_FILE *file = NULL;
	void *data = NULL;
	struct uade_state *state = NULL;

	assert(subsong >= -1);

	file = deadbeef->fopen(fname);
	if (!file) {
		error("Can not open file\n");
		goto error;
	}

	size = deadbeef->fgetlength(file);
	if (!size) {
		info("Filesize 0: %s\n", fname);
		goto error;
	}

	data = malloc(size);
	if (data == NULL) {
		error("Unable to allocate %zu for %s\n",
			size, fname);
		goto error;
	}

	nmemb = deadbeef->fread(data, size, 1, file);

	deadbeef->fclose(file);
	file = NULL;

	if (nmemb == 0) {
		error("Could not read data of %s\n", fname);
		goto error;
	}

	if (!uade_is_rmc(data, size)) {
		info("%s is not an rmc file. The file must have changed "
		     "since it was added to the play list.\n", fname);
		goto error;
	}

	state = uade_new_state(NULL);
	if (state == NULL) {
		error("Could not initialize uade state\n");
		goto error;
	}

	if (uade_play_from_buffer(NULL, data, size, subsong, state) <= 0) {
		error("Can not play\n");
		goto error;
	}

	free(data);

	return state;

error:
	if (data)
		free(data);
	if (file)
		deadbeef->fclose(file);
	if (state)
		uade_cleanup_state(state);
	return NULL;
}

/* deadbeef_uade_init() is called once when song playback is started */
static int deadbeef_uade_init(DB_fileinfo_t *_info, DB_playItem_t *it)
{
	struct uade_info_t *info = (struct uade_info_t *) _info;
	const char *fname = get_fname(it);
	int subsong = deadbeef->pl_find_meta_int(it, ":TRACKNUM", 0);
	if (subsong < 0) {
		fprintf(stderr, "VERY WEIRD. Subsong was set to a negative "
			"value. This is not good. Setting to default "
			"choice.\n");
		subsong = -1;
	}

	info->state = play_file(fname, subsong);
	if (info->state == NULL)
		return -1;

	_info->plugin = &plugin;
	_info->readpos = 0;

	_info->fmt.bps = 16; /* 16 bits per sample is a constant for uade */
	_info->fmt.channels = 2;
	_info->fmt.channelmask = 0x3; /* Enable two channels */
	_info->fmt.samplerate = uade_get_sampling_rate(info->state);
	return 0;
}

/* Free everything allocated in _init */
static void deadbeef_uade_free(DB_fileinfo_t *_info) {
	struct uade_info_t *info = (struct uade_info_t *) _info;
	if (info == NULL)
		return;
	uade_cleanup_state(info->state);
	free (info);
}

/* Returns 0 on song end or error */
static int deadbeef_uade_read(DB_fileinfo_t *_info, char *bytes, int size)
{
	struct uade_info_t *info = (struct uade_info_t *) _info;
	ssize_t readsize = uade_read(bytes, size, info->state);
	struct uade_notification n;
	enum uade_notification_type type;

	if (readsize < 0) {
		info("uade_read() error.\n");
		return 0;
	}

	while (uade_read_notification(&n, info->state)) {
		type = n.type;
		uade_cleanup_notification(&n);
		if (type == UADE_NOTIFICATION_SONG_END)
			return 0;
	}

	return readsize;
}

static int deadbeef_uade_seek_sample(DB_fileinfo_t *_info, int sample)
{
	struct uade_info_t *info = (struct uade_info_t *) _info;
	_info->readpos = (float) sample / _info->fmt.samplerate;
	fprintf(stderr, "Seeking subsong should be broken.\n");
	return uade_seek_samples(UADE_SEEK_SONG_RELATIVE, sample, 0,
				 info->state);
}

static int deadbeef_uade_seek(DB_fileinfo_t *_info, float seconds)
{
	int sample = seconds * _info->fmt.samplerate;
	return deadbeef_uade_seek_sample(_info, sample);
}

/*
 * deadbeef_uade_insert() is called once for each song when the song is added
 * on the playlist
 */
static DB_playItem_t *deadbeef_uade_insert(ddb_playlist_t *plt,
					   DB_playItem_t *after,
					   const char *fname)
{
	struct uade_state *state;
	DB_playItem_t *it;
	double total_duration;
	const struct uade_song_info *info;
	int subsong_min;
	int subsong_max;
	int subsong;
	struct bencode *container;
	const struct bencode *durations;

	state = play_file(fname, -1);
	if (state == NULL)
		return NULL;

	/* Get relevant metrics and info from the song */
	container = uade_get_rmc_from_state(state);
	durations = uade_rmc_get_subsongs(container);
	total_duration = uade_rmc_get_song_length(container);
	if (total_duration <= 0) {
		total_duration = 512;
		fprintf(stderr, "rmc has a bad song length: %f File: %s\n",
			total_duration, fname);
	}

	info = uade_get_song_info(state);
	subsong_min = info->subsongs.min;
	subsong_max = info->subsongs.max;

	for (subsong = subsong_min; subsong <= subsong_max; subsong++) {
		double duration;
		struct bencode *b_duration;

		it = deadbeef->pl_item_alloc_init(fname, plugin.plugin.id);
		deadbeef->pl_add_meta(it, ":FILETYPE", "rmc");

		b_duration = ben_dict_get_by_int(durations, subsong);
		if (b_duration != NULL) {
			duration = ben_int_val(b_duration) / 1000.0;
		} else {
			fprintf(stderr, "rmc container is broken. It does not "
				"contain durations for all subsong. "
				"The culprit file is %s.\n", fname);
			duration = total_duration;
		}
		if (duration <= 0) {
			fprintf(stderr, "rmc container is broken. It has an "
				"invalid subsong (%d) length: %f. File: %s\n",
				subsong, duration, fname);
			duration = total_duration;
		}
		deadbeef->plt_set_item_duration(plt, it, duration);

		deadbeef->pl_add_meta(it, ":CHANNELS", "2");
		deadbeef->pl_set_meta_int(it, ":TRACKNUM", subsong);

		/*snprintf(track, sizeof track, "%d", subsong + 1);
		  deadbeef->pl_add_meta(it, "track", track);*/

		after = deadbeef->plt_insert_item(plt, after, it);
		deadbeef->pl_item_unref(it);
		it = NULL;
	}

	/* Delete the uade context */
	uade_cleanup_state(state);
	state = NULL;

	return after;
}

static int deadbeef_uade_start(void)
{
	/* This is called after all the modules have been loaded */
	return 0;
}

static int deadbeef_uade_stop(void)
{
	return 0;
}

static const char *exts[] = {"rmc", NULL};

static DB_decoder_t plugin = {
	DB_PLUGIN_SET_API_VERSION
	.plugin.version_major = 1,
	.plugin.version_minor = 0,
	.plugin.api_vmajor = 1,
	.plugin.api_vminor = 0,
	.plugin.type = DB_PLUGIN_DECODER,
	.plugin.id = "deadbeef_uade",
	.plugin.name = "uade",
	.plugin.descr =

	"Amiga music plugin\n"
	"\n"
	"Needs libuade that is newer than\n"
	"2013-05-05 from https://gitlab.com/uade-music-player/uade\n"
	"\n"
	"Note, this can only play amiga files\n"
	"that are wrapped in RMC (retro music\n"
	"container) files.\n"
	"\n"
	"git clone https://gitlab.com/uade-music-player/rmc for\n"
	"rmc tool that wraps native Amiga song\n"
	"files into an RMC.\n",

	.plugin.copyright =
	    "libuade plugin for DeaDBeeF Player\n"
	    "Copyright (C) 2013 Heikki Orsila\n",
	.plugin.website = "http://zakalwe.fi/uade",
	.exts = exts,
	.plugin.start = deadbeef_uade_start,
	.plugin.stop = deadbeef_uade_stop,
	.open = deadbeef_uade_open,
	.init = deadbeef_uade_init,
	.free = deadbeef_uade_free,
	.read = deadbeef_uade_read,
	.seek = deadbeef_uade_seek,
	.seek_sample = deadbeef_uade_seek_sample,
	.insert = deadbeef_uade_insert,
};

DB_plugin_t *
deadbeef_uade_load (DB_functions_t *api) {
	/* This is called immediately after plugin loading */
	deadbeef = api;
	return DB_PLUGIN(&plugin);
}
